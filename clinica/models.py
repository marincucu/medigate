from __future__ import unicode_literals

from django.db import models

from nomenclatoare import models as nomenclatoare

class Adresa(models.Model):
    tara                    = models.ForeignKey(nomenclatoare.Tara)
    judet                   = models.ForeignKey(nomenclatoare.Judet)
    oras                    = models.ForeignKey(nomenclatoare.Oras)
    strada                  = models.ForeignKey(nomenclatoare.Strada)

class Contact(models.Model):
    telefon                  = models.CharField(max_length = 20)
    email                    = models.EmailField()

class Pacient(models.Model):
    nume                     = models.CharField(max_length = 200)
    prenume                  = models.CharField(max_length = 200)
    cnp                      = models.CharField(max_length = 13)
    adresa                   = models.ForeignKey(Adresa)
    contact                  = models.ForeignKey(Contact)

class Medic(models.Model):
    nume = models.CharField(max_length=200)
    prenume = models.CharField(max_length=200)
    specialitate = models.ForeignKey(nomenclatoare.SpecialitateMedic)
    grad = models.ForeignKey(nomenclatoare.GradMedic)

class Programare(models.Model):
    pacient = models.ForeignKey(Pacient)
    ora = models.DateTimeField(auto_now=False)
    durata = models.DurationField()
    medic = models.ForeignKey(Medic)

#class Utilizator(models.Model):
    #fel = models.OneToOneModelField(settings.AUTH_USER_MODEL)
    
