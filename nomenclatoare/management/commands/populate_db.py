#-*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
#from nomenclatoare.models import *
#from lxml import etree
import psycopg2
import xml.sax
#oare conteaza si cine depide de cine?
"""
nomenclatoare_parinte=[countries, cityType, streetType, insuranceHouseTypes, personalIdCardTypes, pharmaceuticalForm, concentrations, specialitties,degrees, errors, personCategory, PersonState, prescriptionTypes, physician, physicianSpeciality, NHP, DiseaseCategory, icd10, cim10, ActiveSubstance, CopaymentListTypes,  Act, MedSrvPack, ClinicServiceGroup, SickLeaveIndemnizationsGroup, InfectoContagiousDisease, MedicoChirurgicalEmergency, OrgUnitReportingMapping, DailyService, PersonTypes, ChronicDiseasesCategory, PersonFunction, EmplType, CtrDocumentTypes, InvoiceItem]
nomenclatoare_cu_parinte=[cnasAgreement, euMember, district, city, insuranceHouse, ActSubstICD10, CpmetLstTypePersState,ClinicService, SickLeaveIndemnization, ChronicDisease] 
nomenclatoare_cu_mai_multi_parinti=[ street, drug, CopaymentListDrug,ClinSrvMedPack, LabSrvMedPack, DailySrvMedPack] 
"""


class Command(BaseCommand):
    help='Populează baza de date cu căcatu de xml de la CNAS'
    def __init__(self):
        self.db = psycopg2.connect("dbname=medigateDB user=cuci")
        self.cur = self.db.cursor()
        self.cur.execute("CREATE TABLE Country (code varchar, name varchar)") #mda o sa folosesc doar varchar
        self.cur.execute("""CREATE TABLE CnasAgreeement (countrycode varchar, validFrom date, validTo date);
            CREATE TABLE EuMember (countryCode varchar, validFrom date, validTo date);
            CREATE TABLE District (code varchar, name varchar, country varchar);
            CREATE TABLE CityType (code varchar, name varchar, urbanFlag boolean);
            CREATE TABLE City (code varchar, name varchar, district varchar, cityType varchar, parentCity varchar);
            CREATE TABLE Street_Type (code varchar, name varchar);
            CREATE TABLE Street (code varchar, name varchar, city_code varchar, streetType varchar);
            CREATE TABLE InsuranceHouseType (code varchar, description varchar);
            CREATE TABLE InsuranceHouse (code varchar, name varchar, type varchar, validFrom date, validTo date);
            CREATE TABLE PersonalIDCardType (code varchar, name varchar, category varchar, validFrom date, validTo date);
            CREATE TABLE PharmaceuticalForm (code varchar, validFrom date, validTo date);
            CREATE TABLE Concentration (concentration varchar, validFrom date, validTo date);
            CREATE TABLE Speciality (code varchar, name varchar, validFrom date, validTo date);
            CREATE TABLE Degree (code varchar, description varchar, extraPointsPercent varchar, validFrom date, validTo date);
            CREATE TABLE Error (code varchar, text varchar, validFrom date, validTo date);
            CREATE TABLE PersonCategory (code varchar, description varchar, validFrom date, validTo date, restrictedMinAge varchar,
                                        restrictedMaxAge varchar, supportsOverlapping boolean, definedByAge boolean, maxDuration varchar, maxDurationRoundType varchar,
                                        forSex varchar, personState varchar, priority varchar, isOptional boolean, canBeReported boolean, equivalentCode varchar);
            CREATE TABLE PersonState (code varchar, description varchar, validFrom date, validTo date);
            CREATE TABLE PrescriptionType (code varchar, description varchar, forNarcotics boolean, validFrom date, validTo date);
            CREATE TABLE Physician (pid varchar, name varchar, stencil varchar, validFrom date, validTo date);
            CREATE TABLE PhysicianSpeciality (stencil varchar, contractNo varchar, insuranceHouse varchar, contractType varchar, physicianType varchar, 
                                            specialityCode varchar, validFrom date, validTo date);
            CREATE TABLE Nhp (code varchar, description varchar, validFrom date, validTo date, hasAmbulatoryBudget boolean, hasHospitalBudget boolean,
                            hasDrugsBudget boolean, hasGoodsBudget boolean, programCode varchar);
            CREATE TABLE DiseaseCategory (code varchar, description varchar, isChronicDisease boolean, isAuctioned boolean, validFrom date, validTo date);
            CREATE TABLE Icd10 (code varchar, name varchar, diseaseCategory varchar, validFrom date, validTo date);
            CREATE TABLE Cim10 (code varchar, name varchar, entityLevel varchar, parentCode varchar);
            CREATE TABLE ActiveSubstance (code varchar, validFrom date, validTo date);
            CREATE TABLE ActSubstICD10 (activeSubstance varchar, icd10 varchar, validFrom date, validTo date);
            CREATE TABLE CopaymentListType (code varchar, description varchar, percent varchar, drugMaxNo varchar, prescriptionMaxNo varchar, maxValue varchar,
                            validFrom date, validTo date);
            CREATE TABLE CpmetLstTypePersState ( copaymentListType varchar, personState varchar, percent varchar, validFrom date, validTo date);
            CREATE TABLE Atc (code varchar, description varchar, validFrom date, validTo date, parentATC varchar);
            CREATE TABLE DocumentFormEu (formEuCode varchar, formEuDesc varchar, formEuCateg varchar);
            CREATE TABLE Drug (code varchar, name varchar, presentationMode varchar, isNarcotic varchar, isFractional boolean, isSpecial boolean,
                                isBrand boolean, hasBioEchiv boolean, qtyPerPackage varchar, pricePerPackage varchar, wholeSalePricePerPackage varchar,
                                prescriptionMode varchar, validFrom date, validTo date, activeSubstance varchar, concentration varchar, pharmaceuticalForm varchar,
                                company varchar, country varchar, atc varchar);
            CREATE TABLE CopaymentListDrug (copaymentListType varchar, drug varchar, nhpCode varchar, diseaseCode varchar, maxPrice varchar, maxPriceUT varchar,
                                wholeSalePrice varchar, copaymentValue varchar, copaymentValue90 varchar, referencePrice varchar, specialLaw boolean, needApproval varchar,
                                overValue varchar, needSpecialty varchar, classifInsulin varchar, hgDci varchar, hgAtc varchar, validFrom date, validTo date);
            CREATE TABLE MedSrvPack (code varchar, name varchar, validFrom date, validTo date);
            CREATE TABLE ClinicServiceGroup (code varchar, description varchar, validFrom date, validTo date);
            CREATE TABLE ClinSrvGrpSpecialties (serviceGroup varchar, specialty varchar, validFrom date, validTo date);
            CREATE TABLE ClinicService (code varchar, name varchar, serviceGroup varchar, isExam boolean, opRoom boolean, isConnectedService boolean,
                                    validFrom date, validTo date);
            CREATE TABLE ClinSrvMedPack (service varchar, medSrvPack varchar, validFrom date, validTo date, points varchar, pointsCtrl varchar, pointsSrg varchar,
                                    valueLei varchar);
            CREATE TABLE LaboratoriesService (code varchar, name varchar, highLevel boolean, recByPC boolean, formula boolean, inClinic boolean, validFrom date,
                                    validTo date, serviceType varchar, reportOrder varchar);
            CREATE TABLE LabSrvMedPack (service varchar, medSrvPack varchar, validFrom date, validTo date);
            CREATE TABLE SickLeaveIndemnizationsGroup (code varchar, description varchar, validFrom date, validTo date);
            CREATE TABLE SickLeaveIndemnization (code varchar, description varchar, indemnizationGroup varchar, percent varchar, isTaxable boolean, mustHaveStage boolean, 
                                    validFrom date, validTo date);
            CREATE TABLE InfectoContagiousDisease (code varchar, description varchar, validFrom date, validTo date);
            CREATE TABLE MedicoChirurgicalEmergency (code varchar, description varchar, validFrom date, validTo date);
            CREATE TABLE OrgUnitReportingMapping (orgUnitCode varchar, reportingCode varchar, validFrom date, validTo date, appType varchar);
            CREATE TABLE DailyService (code varchar, description varchar, validFrom date, validTo date);
            CREATE TABLE DailySrvMedPack (code varchar, medSrvPack varchar, validFrom date, validTo date, tariff varchar);
            CREATE TABLE PersonType (code varchar, description varchar, isForeign boolean);
            CREATE TABLE ChronicDiseasesCategory (code varchar, description varchar, validFrom date, validTo date);
            CREATE TABLE ChronicDisease (code varchar, description varchar, category varchar, orderNo varchar, validFrom date, validTo date);
            CREATE TABLE PersonFunction (code varchar, description varchar, validFrom date, validTo date);
            CREATE TABLE EmplType (code varchar, name varchar);
            CREATE TABLE CtrDocumentType (code varchar, name varchar, validFrom date, validTo date);
            CREATE TABLE InvoiceItem (code varchar, description varchar, providerCategory varchar, contractType varchar, validFrom date, validTo date);
            """.replace('\n',' '))
        self.db.commit()
        self.parser = NomenclatorHandler(self.cur)
        self.source=open("../nomenclatoare-siui/NomenclatoareAMB_20150408.xml")



    def handle(self, *args, **options): 
        xml.sax.parse(self.source, self.parser, self.cur)
        self.db.commit()
        self.cur.close()
        self.db.close()



class NomenclatorHandler(xml.sax.ContentHandler):

    def __init__(self, cursor):
        xml.sax.ContentHandler.__init__(self)
        self.cursor = cursor

    def cueri_cool(self, tabel, coloane, valori): #aici 3 step aproach
        query = 'insert into {0} ({1}) values ({2})'.format(tabel, ', '.join(coloane), ",".join(['%s']*len(valori)))
        self.cursor.execute(query, (valori))



    def startElement(self, name, attrs):
        if name=="Country":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="CnasAgreeement":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="EuMember":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="District":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="CityType":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="City":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="Street_Type":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="Street":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="InsuranceHouseType":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="InsuranceHouse":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="PersonalIdCardType":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="PharmaceuticalForm":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="Concentration":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="Speciality":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="Degree":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="Error":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="PersonCategory":
            self.cueri_cool(name, attrs.keys(), attrs.values())
            
        elif name=="PersonState":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="PrescriptionType":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="Physician":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="PhysicianSpeciality":
           self.cueri_cool(name, attrs.keys(), attrs.values()) 
        elif name=="NHP":
           self.cueri_cool(name, attrs.keys(), attrs.values()) 
        elif name=="DiseaseCategory":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="ICD10":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="CIM10":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="ActiveSubstance":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="ActSubstICD10":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="CopaymantListType":
           self.cueri_cool(name, attrs.keys(), attrs.values()) 
        elif name=="CpmetLstTypePersState":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="ATC":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="DocumentFormEu":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="Drug":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="CopaymentListDrug":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="MedSrvPack":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="ClinicServiceGroup":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="ClinSrvGrpSpecialty":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="ClinicService":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="ClinSrvMedPack":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="LaboratoiesService":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="LabSrvMedPack":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="SickLeaveIndemnizationsGroup":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="SickLeaveIndemnization":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="InfectoContagiousDisease":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="MedicoChirurgicalEmergency":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="OrgUnitReportingMapping":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="DailyService":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="DailySrvMedPack":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="PersonType":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="ChronicDiseasesCategory":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="ChronicDisease":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="PersonFunction":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="EmplType":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="CtrDocumentType":
            self.cueri_cool(name, attrs.keys(), attrs.values())
        elif name=="InvoiceItem":
            self.cueri_cool(name, attrs.keys(), attrs.values())
