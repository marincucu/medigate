from django.core.management.base import BaseCommand
from django.db import connection
from nomenclatoare.models import *

class Command(BaseCommand):
    help='populeaza modelele din baza de date prelucrand datele postgres de la CNAS'

    def __init__(self):
        self.cur = connection.cursor()

    def handle(self, *args, **options):
        self.baga_tabele_fara_parinte()
        self.baga_tabele_cu_un_parinte()

    def baga_tabele_fara_parinte(self):
        self.baga_tari()
        self.baga_judete()
        self.baga_tip_oras()
        self.baga_tip_strada()
        self.baga_specialitati_si_grad()
        self.baga_fel_personal_medical()

    def baga_tabele_cu_un_parinte(self):
        self.baga_orase()
        self.baga_strazi()

    def baga_specialitati_si_grad(self):
        self.cur.execute(""" SELECT * FROM speciality""")
        specialitati = self.cur.fetchall()
        for specialitate in specialitati:
            cod = specialitate[0]
            nume = specialitate[1]
            Specialitate.objects.create(cod=cod, nume = nume)
        self.cur.execute(""" SELECT * from degree """)
        grade = self.cur.fetchall()
        for grad in grade:
            cod = grad[0]
            nume = grad[1]
            puncteextra=grad[2]
            Grad.objects.create(cod= cod, nume=nume,puncteextra=puncteextra)


    def baga_tip_oras(self):
        self.cur.execute(""" SELECT * FROM cityType """)
        tipuri_oras = self.cur.fetchall()
        for tip_oras in tipuri_oras:
            cod = tip_oras[0]
            nume = tip_oras[1]
            urban_flag = tip_oras[2]
            TipOras.objects.create(cod=cod, nume=nume, urban_flag=urban_flag)

    def baga_tip_strada(self):
        self.cur.execute(""" SELECT * from street_type """)
        tipuri_strazi = self.cur.fetchall()
        for tip_strada in tipuri_strazi:
            cod = tip_strada[0]
            nume = tip_strada[1]
            TipStrada.objects.create(cod=cod, nume=nume)


    def baga_tari(self):
        self.cur.execute(""" SELECT * FROM country""") #is doar 109
        tari = self.cur.fetchall()
        for tara in tari:
            cod = tara[0]
            nume = tara[1]
            Tara.objects.create(cod=cod, nume=nume)

    def baga_judete(self):
        self.cur.execute(""" SELECT * from district """) #is doar 42
        judete = self.cur.fetchall()
        for judet in judete:
            cod = judet[0]
            nume = judet[1]
            Judet.objects.create(cod=cod, nume=nume)

    def baga_orase(self):
        self.cur.execute(""" SELECT * FROM city""")
        while True:
            orase = self.cur.fetchmany(5000)
            if not orase:
                break
            for oras in orase:
                cod = oras[0]
                nume = oras[1]
                tip_oras = TipOras.objects.get(cod=oras[3])
                oras_parinte = oras[4]
                judet = Judet.objects.get(cod=oras[2])
                orasNou = Oras.objects.create(cod=cod, nume=nume, judet=judet, tip_oras=tip_oras)

    def baga_strazi(self):
        self.cur.execute(""" SELECT * FROM street""")
        while True:
            strazi = self.cur.fetchmany(5000)
            if not strazi:
                break
            for strada in strazi:
                cod = strada[0]
                nume = strada[1]
                cod_oras = Oras.objects.get(cod = strada[2])
                tip_strada = TipStrada.objects.get(cod = strada[3])
                stradaNoua = Strada.objects.create(cod=cod, nume=nume, oras=cod_oras, tip_strada=tip_strada)

    def baga_fel_personal_medical(self):
        self.cur.execute(""" SELECT * from personfunction""")
        feluri_personal_medical = self.cur.fetchall()
        for fel_personal_medical in feluri_personal_medical:
            cod = fel_personal_medical[0]
            nume = fel_personal_medical[1]
            FelPersonalMedical.objects.create(cod=cod, nume=nume)

