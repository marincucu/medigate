#-*- coding: utf-8 -*-
from django.db import models

class Tara(models.Model):
    nume=models.CharField(max_length=50)
    cod=models.CharField(max_length=15, db_column='code')
    def __str__(self):
        return self.nume

class TipOras(models.Model):

    cod=models.CharField(max_length=80, db_column='code')
    nume=models.CharField(max_length=80, db_column='name')
    urban_flag = models.BooleanField()
    def __str__(self):
        return self.nume

class TipStrada(models.Model):

    cod=models.CharField(max_length=30, db_column='code', primary_key=True)
    nume=models.CharField(max_length=30, db_column='name')
    def __str__(self):
        return self.nume

class Judet(models.Model):

    nume = models.CharField(max_length = 20, db_column  = 'name')
    cod  = models.CharField(max_length = 4, db_column = 'code') #tari id e inutil ca e doar romania, dar nu stiu daca trebuie declarat si in model
    def __str__(self):
        return self.nume

class Oras(models.Model):
    cod      = models.CharField(max_length = 80)
    nume     = models.CharField(max_length = 80)
    judet = models.ForeignKey(Judet)
    tip_oras = models.ForeignKey(TipOras)
    def __str__(self):
        return self.nume

class Strada(models.Model):
    nume       = models.CharField(max_length  = 120)
    cod        = models.CharField(max_length  = 120)
    oras       = models.ForeignKey(Oras)
    tip_strada = models.ForeignKey(TipStrada)

    def __str__(self):
        return self.nume


class FelPersonalMedical(models.Model): #și ăsta are valid from
    cod = models.CharField(max_length = 60)
    nume = models.CharField(max_length = 200)

class SpecialitateMedic(models.Model): #cum fac cu valid to și valid from
    cod = models.CharField(max_length=120)
    nume = models.CharField(max_length=120)
    puncteextra = models.CharField(max_length=3)

class GradMedic(models.Model):
    cod = models.CharField(max_length=120)
    nume = models.CharField(max_length=120)
    puncteextra=models.CharField(max_length=2)


