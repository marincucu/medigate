from rest_framework import serializers
from nomenclatoare import models as nomenclatoare

class TariSerializer(serializers.ModelSerializer):
    class Meta:
        model = nomenclatoare.Tara
        fields = ('id','nume','cod')
