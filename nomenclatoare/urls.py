from django.conf.urls import url
from nomenclatoare import views

urlpatterns=[
        url(r'^tari/$', views.TariList.as_view(), name='tari-toate'),
        url(r'^tari/(?P<nume>\w+)/$', views.TaraList.as_view(), name='cautare-tara'),
        url(r'^judet/(?P<nume>\w+)/$', views.Judet.as_view(), name='cautare-judet'),
        ]

