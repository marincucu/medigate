from nomenclatoare import models as nomenclatoare
from nomenclatoare.serializers import TariSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.generics import RetrieveAPIView
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework import status

class TariList(APIView):
    """
    vezi toaaaate tarile
    """
    def get(self, request, format=None):
        tari = nomenclatoare.Tara.objects.all()
        serializer = TariSerializer(tari, many=True)
        return Response(serializer.data)

class TaraList(APIView):
    """
    vezi doar o tara
    """
    def get_object(self, nume):
        try:
            return nomenclatoare.Tara.objects.get(nume=nume)
        except nomenclatoare.Tara.DoesNotExist:
            raise Http404

    def get(self, request, nume, format=None):
        tara = self.get_object(nume)
        serializer = TariSerializer(tara)
        return Response(serializer.data)


class Judet(ListAPIView):
    #print(nume)
    #lookup_field='nume'
    #queryset = nomenclatoare.Judet.objects.all()
    def retrive(request, *args, **kwargs):
        try:
            return nomenclatoare.Judet.objects.get(nume=request.nume)
        except nomenclatoare.Judet.DoesNotExist:
            raise Http404
